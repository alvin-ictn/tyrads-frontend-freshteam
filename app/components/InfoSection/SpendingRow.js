import Image from "next/image";

export default function SpendingRow({ history, ...rest }) {
  return (
    <div className="transaction-row" {...rest}>
      <div className="user">
        <div className="thumbnail" alt="user-picture">
          <Image src={history.picture} />
        </div>
        <div className="user-info">
          <span className="name">{history.name}</span>
          <span className="date">{history.record_date}</span>
        </div>
      </div>
    </div>
  );
}
