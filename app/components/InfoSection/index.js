import InfoWrapper from "./InfoWrapper";
import styles from "./css/infoSection.module.css";
import shopping from "@/public/assets/images/shopping.svg";
import hospital from "@/public/assets/images/hospital.svg";
import ticket from "@/public/assets/images/ticket.svg";
import SpendingRow from "./SpendingRow";

const expenseIncomeData = {
  expense: {
    data: 7500,
    text: "7,500",
  },
  income: {
    data: 2500,
    text: "2,500",
  },
};

const spendingData = [
  {
    id: 1,
    name: "Online store",
    picture: shopping,
    record_date: "06 July, 2023 at 10:00 pm",
  },
  {
    id: 2,
    name: "Pay the hospital",
    picture: hospital,
    record_date: "04 July, 2023 at 10:00 pm",
  },
  {
    id: 3,
    name: "Tickets",
    picture: ticket,
    record_date: "03 July, 2023 at 10:00 pm",
  },
];

export default function InfoSection({ className, children }) {
  return (
    <section className={styles["info-section"]}>
      <InfoWrapper>
        <header>
          <h5 className={styles.title}>Expense and Income</h5>
        </header>
        <div className={styles["expense-income-info"]}>
          <div
            className={`${styles["expense-income-info-item"]} ${styles.left}`}
          >
            <p>Expense</p>
            <h4 className={styles.percentage}>
              {(expenseIncomeData.expense.data /
                (expenseIncomeData.expense.data +
                  expenseIncomeData.income.data)) *
                100}
              %
            </h4>
            <p>{expenseIncomeData.expense.text}</p>
          </div>
          <div
            className={`${styles["expense-income-info-item"]} ${styles.center}`}
          >
            <div className={styles["line"]}></div>
            <h4 className={styles.percentage}>vs</h4>
            <div className={styles["line"]}></div>
          </div>
          <div
            className={`${styles["expense-income-info-item"]} ${styles.right}`}
          >
            <p>Income</p>
            <h4 className={styles.percentage}>
              {(expenseIncomeData.income.data /
                (expenseIncomeData.expense.data +
                  expenseIncomeData.income.data)) *
                100}
              %
            </h4>
            <p>{expenseIncomeData.income.text}</p>
          </div>
        </div>
        <div>
          <div className={styles["bar-chart"]}>
            <div
              style={{
                width: `${
                  (expenseIncomeData.expense.data /
                    (expenseIncomeData.expense.data +
                      expenseIncomeData.income.data)) *
                  100
                }%`,
              }}
              className={styles.expense}
            ></div>
            <div
              style={{
                width: `${
                  (expenseIncomeData.income.data /
                    (expenseIncomeData.expense.data +
                      expenseIncomeData.income.data)) *
                  100
                }%`,
              }}
              className={styles.income}
            ></div>
          </div>
        </div>
      </InfoWrapper>
      <InfoWrapper>
        <header className={styles["spending-title"]}>
          <h5>Latest spending</h5>
          <div>
            <i class="ri-more-2-fill"></i>
          </div>
        </header>
        <div className={styles["spending-table"]}>
          {spendingData.map((spending) => (
            <SpendingRow
              key={spending.id}
              history={spending}
              style={{ width: "100%" }}
            />
          ))}
        </div>
        <div className={styles["more-spending"]}>
          <p className={styles["more-spending"]}>View all</p>
          <i className="ri-arrow-right-line"></i>
        </div>
      </InfoWrapper>
      <InfoWrapper>
        <header>
          <h5 className={`${styles.title} ${styles["text-center"]}`}>
            Go to preimum
          </h5>
        </header>
        <div className={styles["premium-feature"]}>
          <p className={styles["more-feature"]}>Need more features?</p>
          <p className={styles.info}>
            Update your account to premium to get more features
          </p>
        </div>
        <button>Get now</button>
      </InfoWrapper>
    </section>
  );
}
