import styles from "./css/InfoWrapper.module.css";

export default function InfoWrapper({ children }) {
  return <div className={styles.wrapper}>{children}</div>;
}
