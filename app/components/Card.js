export default function Card({className, children, ...rest}) {
    return (
        <div className={`${className || ""} card`} {...rest}>
            {children || <></>}
        </div>
    );
}
