"use client";
import localFont from "next/font/local";
import Image from "next/image";
import {useState} from "react";
import "remixicon/fonts/remixicon.css";
import Analytics from "./components/Analytics";
import BalanceStatisic from "./components/BalanceStatistic";
import Card from "./components/Card";
import InfoSection from "./components/InfoSection";
import Transaction from "./components/Transaction";
import visa from "@/public/assets/images/visa.jpg"
import "./style.css";

const data = [
  { name: "Uncategorize", value: 60, color: "#f1f1f1" },
  { name: "To Do", value: 80, color: "#eb6252" },
  { name: "In Progress", value: 80, color: "#ffa512" },
  { name: "Done", value: 60, color: "#535ceb" },
];

const origoPro = localFont({
  src: [
    {
      path: "../public/assets/font/OrigoProRegular.ttf",
      weight: "400",
      style: "normal",
    },
    {
      path: "../public/assets/font/OrigoProMedium.ttf",
      weight: "500",
      style: "normal",
    },
    {
      path: "../public/assets/font/OrigoProSemiBold.ttf",
      weight: "600",
      style: "normal",
    },
    {
      path: "../public/assets/font/OrigoProBold.ttf",
      weight: "700",
      style: "normal",
    },
    {
      path: "../public/assets/font/OrigoProExtraBold.ttf",
      weight: "800",
      style: "normal",
    },
  ],
});

const sideMenu = [{ name: "" }];

export default function Home() {
  const [activeMenu, setActiveMenu] = useState("home");

  return (
    <main className={origoPro.className}>
      <aside className="sidebar">
        <div className="session">
          <p>S.</p>
        </div>

        <div className="bubble-icon bg-chat">
          <i className="ri-question-answer-line"></i>
        </div>

        <div></div>
        <div></div>

        <nav className="side-menu bg-menu">
          <div className="menu active">
            <i class="ri-home-6-line"></i>
          </div>
          <div className="menu">
            <i class="ri-notification-2-line"></i>
          </div>
          <div className="menu">
            <i class="ri-time-line"></i>
          </div>
          <div className="menu">
            <i class="ri-group-line"></i>
          </div>
          <div className="menu">
            <i class="ri-wallet-line"></i>
          </div>
          <div className="menu">
            <i class="ri-settings-2-line"></i>
          </div>
        </nav>

        <div></div>
        <div></div>

        <div>
          {/* <div className="bubble-icon bg-menu">
            <i class="ri-logout-box-r-line"></i>
          </div> */}
          <div className="bubble-icon bg-menu">
            <i class="ri-logout-box-r-line"></i>
          </div>
        </div>
      </aside>
      <section className="main-content">
        <div>
          <p className="hello">Hello, Jhon .D</p>
        </div>
        <div className="transaction">
          <Card className="flex-1">
            <BalanceStatisic />
          </Card>
          <Card>
            <Image src={visa} alt="visa" height={200} />
          </Card>
        </div>
        <div styles={{height: "100%"}}>
            <Card>
                <div className="dummy"></div>
            </Card>
        </div>
        <div className="transaction">
          <Card className="transaction-card flex-1">
            <div className="transaction-title">
              <p>Last transactions</p>
              <div className="transaction-sort">
                <div>
                  <span>Newest</span>
                </div>
                <div>
                  <span>Oldest</span>
                </div>
              </div>
            </div>
            <Transaction />
          </Card>
          <Card style={{ position: "relative" }}>
            <header>
              <p className="card-title">Analytics</p>
            </header>
            <Analytics data={data} />
          </Card>
        </div>
      </section>
      <InfoSection />
    </main>
  );
}
